package vsmcli_test

const exampleMapJSON = `{
  "resources": [
    {
      "type": "process",
      "title": "Sign off\nchange\ncontrol\n",
      "process": "0874234",
      "description": "manual review\nand sign off of\nsubmitted change\n",
      "metrics": {
        "leadtime": "48h",
        "processtime": "24h",
        "LeadTimeDuration": 0,
        "ProcessTimeDuration": 0,
        "ActivityRatio": "",
        "c\u0026aratio": "60%",
        "c\u0026aratio_desc": "the process often results in a rejection hence our 60% ratio"
      },
      "flow": {},
      "LeadTimeWeight": 0
    },
    {
      "type": "process",
      "title": "automated\nsecurity\ntest\n",
      "process": "0874235",
      "description": "the ",
      "metrics": {
        "leadtime": "24h",
        "processtime": "12h",
        "LeadTimeDuration": 0,
        "ProcessTimeDuration": 0,
        "ActivityRatio": "",
        "c\u0026aratio": "60%",
        "c\u0026aratio_desc": "the process often results in a rejection hence our 60% ratio"
      },
      "flow": {},
      "previous_process": "0874234",
      "LeadTimeWeight": 0
    },
    {
      "type": "process",
      "title": "automated\nperformance\ntest\n",
      "process": "0874236",
      "description": "the ",
      "metrics": {
        "leadtime": "60m",
        "processtime": "55m",
        "LeadTimeDuration": 0,
        "ProcessTimeDuration": 0,
        "ActivityRatio": "",
        "c\u0026aratio": "60%",
        "c\u0026aratio_desc": "the process often results in a rejection hence our 60% ratio"
      },
      "flow": {},
      "previous_process": "0874235",
      "LeadTimeWeight": 0
    }
  ],
  "render_process_keys": false,
  "customer": "Customer",
  "title": "ExampleMap",
  "working_day": "8h",
  "WorkDay": 0,
  "MetricsSummary": { "leadtime": "", "processtime": "", "activityratio": "", "c\u0026aratio": "" }
}
`

const exampleMapJSONVersion = `{
  "resources": [
    {
      "type": "process",
      "title": "Sign off\nchange\ncontrol\n",
      "process": "0874234",
      "description": "manual review\nand sign off of\nsubmitted change\n",
      "metrics": {
        "leadtime": "48h",
        "processtime": "24h",
        "LeadTimeDuration": 0,
        "ProcessTimeDuration": 0,
        "ActivityRatio": "",
        "c\u0026aratio": "60%",
        "c\u0026aratio_desc": "the process often results in a rejection hence our 60% ratio"
      },
      "flow": {},
      "LeadTimeWeight": 0
    },
    {
      "type": "process",
      "title": "automated\nsecurity\ntest\n",
      "process": "0874235",
      "description": "the ",
      "metrics": {
        "leadtime": "24h",
        "processtime": "12h",
        "LeadTimeDuration": 0,
        "ProcessTimeDuration": 0,
        "ActivityRatio": "",
        "c\u0026aratio": "60%",
        "c\u0026aratio_desc": "the process often results in a rejection hence our 60% ratio"
      },
      "flow": {},
      "previous_process": "0874234",
      "LeadTimeWeight": 0
    },
    {
      "type": "process",
      "title": "automated\nperformance\ntest\n",
      "process": "0874236",
      "description": "the ",
      "metrics": {
        "leadtime": "60m",
        "processtime": "55m",
        "LeadTimeDuration": 0,
        "ProcessTimeDuration": 0,
        "ActivityRatio": "",
        "c\u0026aratio": "60%",
        "c\u0026aratio_desc": "the process often results in a rejection hence our 60% ratio"
      },
      "flow": {},
      "previous_process": "0874235",
      "LeadTimeWeight": 0
    }
  ],
  "render_process_keys": false,
  "customer": "Customer",
	"title": "ExampleMap",
	"version": "0.0.1",
  "working_day": "8h",
  "WorkDay": 0,
  "MetricsSummary": { "leadtime": "", "processtime": "", "activityratio": "", "c\u0026aratio": "" }
}
`

const versionsJSON = `[
  "0.0.1",
  "0.1.1",
  "1.0.0"
]
`

const noVersionsJSON = "null"

const noMapJSON = `{
  "error": "map with name: 'test-map' does not exist, cannot list versions"
}
`

const deleteErrJSON = `{
  "error":"map with name: 'test-map' does not exist, cannot list versions"
}
`

const deleteErrVersionJSON = `{
  "error":"Could not delete map 'test-map:0.0.1' as it did not exist, found other versions: [0.1.1, 1.0.0]"
}
`

const getTitlesJSON = `{
  "test1": "An example test",
  "test2": "another example"
}
`

const getTitlesErrJSON = `{
  "error":"Could not get titles"
}
`

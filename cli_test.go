package vsmcli_test

import (
	"os"
	"reflect"
	"runtime"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	cli "github.com/urfave/cli/v2"

	"bitbucket.org/armakuni/value-stream-mapping-cli"
)

var _ = Describe("#CreateCLI", func() {
	It("returns a cli.App", func() {
		app := vsmcli.CreateCLI(os.Stdout)
		Ω(app).Should(BeAssignableToTypeOf(&cli.App{}))
		Ω(app.Commands).Should(HaveLen(7))
		Ω(app.Commands[0].Name).Should(Equal("validate-map"))
		Ω(runtime.FuncForPC(reflect.ValueOf(app.Commands[0].Action).Pointer()).Name()).Should(Equal(
			"bitbucket.org/armakuni/value-stream-mapping-cli.(*Actions).ValidateMap-fm",
		))
		Ω(app.Commands[1].Name).Should(Equal("get-map"))
		Ω(runtime.FuncForPC(reflect.ValueOf(app.Commands[1].Action).Pointer()).Name()).Should(Equal(
			"bitbucket.org/armakuni/value-stream-mapping-cli.(*Actions).GetMap-fm",
		))
		Ω(app.Commands[2].Name).Should(Equal("set-map"))
		Ω(runtime.FuncForPC(reflect.ValueOf(app.Commands[2].Action).Pointer()).Name()).Should(Equal(
			"bitbucket.org/armakuni/value-stream-mapping-cli.(*Actions).SetMap-fm",
		))
		Ω(app.Commands[3].Name).Should(Equal("list-map-versions"))
		Ω(runtime.FuncForPC(reflect.ValueOf(app.Commands[3].Action).Pointer()).Name()).Should(Equal(
			"bitbucket.org/armakuni/value-stream-mapping-cli.(*Actions).ListMapVersions-fm",
		))
		Ω(app.Commands[4].Name).Should(Equal("delete-map"))
		Ω(runtime.FuncForPC(reflect.ValueOf(app.Commands[4].Action).Pointer()).Name()).Should(Equal(
			"bitbucket.org/armakuni/value-stream-mapping-cli.(*Actions).DeleteMap-fm",
		))
		Ω(app.Commands[5].Name).Should(Equal("list-maps"))
		Ω(runtime.FuncForPC(reflect.ValueOf(app.Commands[5].Action).Pointer()).Name()).Should(Equal(
			"bitbucket.org/armakuni/value-stream-mapping-cli.(*Actions).ListMaps-fm",
		))
		Ω(app.Commands[6].Name).Should(Equal("example-map"))
		Ω(runtime.FuncForPC(reflect.ValueOf(app.Commands[6].Action).Pointer()).Name()).Should(Equal(
			"bitbucket.org/armakuni/value-stream-mapping-cli.(*Actions).ExampleMap-fm",
		))
	})
})

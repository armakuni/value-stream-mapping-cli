package vsmcli

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"syscall"

	cli "github.com/urfave/cli/v2"
	"golang.org/x/term"

	"github.com/ghodss/yaml"

	"bitbucket.org/armakuni/value-stream-mapping-ui/mapper"
)

var httpClient http.Client

// Actions - an actions struct containing flags
type Actions struct {
	Flags Flags
}

// Flags - a flags struct containing all usable flags
type Flags struct {
	Map      string
	Username string
	Password string
	URI      string
	Name     string
	Version  string
}

type errResp struct {
	Error string `json:"error"`
}

// ValidateMap - validates that a yaml file is valid
func (actions *Actions) ValidateMap(c *cli.Context) error {
	if actions.Flags.Map == "" {
		cli.ShowCommandHelp(c, "validate-map")
		return cli.NewExitError("map flag must be set", 1)
	}
	if _, err := actions.validate(); err != nil {
		return err
	}
	fmt.Fprintf(c.App.Writer, "value stream map: %s is valid\n", actions.Flags.Map)
	return nil
}

// GetMap - downloads and prints a map.
func (actions *Actions) GetMap(c *cli.Context) error {
	if !actions.validGlobalFlags() {
		cli.ShowAppHelp(c)
		return cli.NewExitError("global flags username, password and uri must be set", 1)
	}

	if actions.Flags.Name == "" {
		cli.ShowCommandHelp(c, "get-map")
		return cli.NewExitError("name flag must be set", 1)
	}

	versionQuery, err := actions.versionQueryString()
	if err != nil {
		return cli.NewExitError(err, 1)
	}

	statusCode, respBody, err := actions.makeRequest("GET",
		fmt.Sprintf("%s/v1/maps/%s%s", actions.Flags.URI, actions.Flags.Name, versionQuery),
		nil,
	)
	if err != nil {
		return cli.NewExitError(err, 1)
	}

	if statusCode != 200 {
		printErr(respBody, c.App.Writer)
		return cli.NewExitError(actions.appendVersionString(
			fmt.Sprintf("could not get value steam map for %s", actions.Flags.Name)), 1)
	}

	var valueStream mapper.ValueStream
	err = json.Unmarshal(respBody, &valueStream)
	if err != nil {
		return cli.NewExitError(err, 1)
	}
	yamlBytes, err := yaml.Marshal(valueStream)
	if err != nil {
		return cli.NewExitError(err, 1)
	}
	fmt.Fprint(c.App.Writer, string(yamlBytes))
	return nil
}

// SetMap - uploading the valuestream map from yaml file
func (actions *Actions) SetMap(c *cli.Context) error {
	if !actions.validGlobalFlags() {
		cli.ShowAppHelp(c)
		return cli.NewExitError("global flags username, password and uri must be set", 1)
	}

	if actions.Flags.Name == "" || actions.Flags.Map == "" {
		cli.ShowCommandHelp(c, "set-map")
		return cli.NewExitError("name and map flags must be set", 1)
	}

	valueStream, err := actions.validate()
	if err != nil {
		return err
	}

	valueStreamJSON, err := json.Marshal(valueStream)
	if err != nil {
		return cli.NewExitError(err, 1)
	}

	statusCode, respBody, err := actions.makeRequest("PUT",
		fmt.Sprintf("%s/v1/maps/%s", actions.Flags.URI, actions.Flags.Name),
		bytes.NewReader(valueStreamJSON),
	)

	if err != nil {
		return cli.NewExitError(err, 1)
	}

	if statusCode != 204 {
		printErr(respBody, c.App.Writer)
		return cli.NewExitError(fmt.Sprintf("could not set value steam map for %s", actions.Flags.Name), 1)
	}
	fmt.Fprintf(c.App.Writer, "map %s has been set", actions.Flags.Name)
	return nil
}

// ListMapVersions - list versions of a map
func (actions *Actions) ListMapVersions(c *cli.Context) error {
	if !actions.validGlobalFlags() {
		cli.ShowAppHelp(c)
		return cli.NewExitError("global flags username, password and uri must be set", 1)
	}

	if actions.Flags.Name == "" {
		cli.ShowCommandHelp(c, "list-map-versions")
		return cli.NewExitError("name flag must be set", 1)
	}

	statusCode, respBody, err := actions.makeRequest("GET",
		fmt.Sprintf("%s/v1/versions/%s", actions.Flags.URI, actions.Flags.Name),
		nil,
	)
	if err != nil {
		return cli.NewExitError(err, 1)
	}

	if statusCode != 200 {
		printErr(respBody, c.App.Writer)
		return cli.NewExitError(fmt.Sprintf("could not get value steam map versions for %s", actions.Flags.Name), 1)
	}

	var versions []string

	if err := json.Unmarshal(respBody, &versions); err != nil {
		return cli.NewExitError(err, 1)
	}

	if len(versions) == 0 {
		fmt.Fprintf(c.App.Writer, "map %s does not have versions", actions.Flags.Name)
		return nil
	}

	for _, version := range versions {
		fmt.Fprintln(c.App.Writer, version)
	}

	return nil
}

// DeleteMap - delete a value stream map
func (actions *Actions) DeleteMap(c *cli.Context) error {
	if !actions.validGlobalFlags() {
		cli.ShowAppHelp(c)
		return cli.NewExitError("global flags username, password and uri must be set", 1)
	}

	if actions.Flags.Name == "" {
		cli.ShowCommandHelp(c, "delete-map")
		return cli.NewExitError("name flag must be set", 1)
	}

	versionQuery, err := actions.versionQueryString()
	if err != nil {
		return cli.NewExitError(err, 1)
	}

	statusCode, respBody, err := actions.makeRequest("DELETE",
		fmt.Sprintf("%s/v1/maps/%s%s", actions.Flags.URI, actions.Flags.Name, versionQuery),
		nil,
	)
	if err != nil {
		return cli.NewExitError(err, 1)
	}

	if statusCode != 200 {
		printErr(respBody, c.App.Writer)
		return cli.NewExitError(actions.appendVersionString(
			fmt.Sprintf("could not delete map %s", actions.Flags.Name)), 1)
	}

	fmt.Fprintln(c.App.Writer, actions.appendVersionString(
		fmt.Sprintf("deleted map %s", actions.Flags.Name)))
	return nil
}

// ListMaps - lists maps with titles
func (actions *Actions) ListMaps(c *cli.Context) error {
	if !actions.validGlobalFlags() {
		cli.ShowAppHelp(c)
		return cli.NewExitError("global flags username, password and uri must be set", 1)
	}

	statusCode, respBody, err := actions.makeRequest("GET",
		fmt.Sprintf("%s/v1/titles", actions.Flags.URI),
		nil,
	)
	if err != nil {
		return cli.NewExitError(err, 1)
	}

	if statusCode != 200 {
		printErr(respBody, c.App.Writer)
		return cli.NewExitError("could not get maps", 1)
	}

	var mapTitles map[string]string
	if err := json.Unmarshal(respBody, &mapTitles); err != nil {
		return cli.NewExitError(err, 1)
	}

	for mapName, mapTitle := range mapTitles {
		fmt.Fprintf(c.App.Writer, "Name: %s, Title: %s\n", mapName, mapTitle)
	}

	return nil
}

// ExampleMap - print an example map
func (actions *Actions) ExampleMap(c *cli.Context) error {
	valueSteam := mapper.ValueStream{
		Customer:       "Mapper",
		Title:          "Example Map",
		Version:        "0.1.0-current",
		WorkWeekString: "5d",
		WorkDayString:  "8h",
		Resources: mapper.Resources{
			&mapper.Resource{
				Type:          "process",
				Title:         "Feature Request",
				Process:       "feature-request",
				Description:   "A user requests a specific feature",
				NumberOfStaff: 2,
				Metrics: &mapper.Metrics{
					LeadTime:    "30m",
					ProcessTime: "25m",
					CARatio:     "90%",
					CARatioDesc: "Sometimes we need to ask for information on potential implementation to size",
				},
			},
			&mapper.Resource{
				Type:            "process",
				Title:           "Create Story",
				Process:         "create-story",
				PreviousProcess: "feature-request",
				Description:     "Based on the feature request a story is created and put in a theme",
				NumberOfStaff:   1,
				Flow:            &mapper.Flow{Out: []string{"issue tracker"}},
				Metrics: &mapper.Metrics{
					LeadTime:    "10m",
					ProcessTime: "10m",
					CARatio:     "95%",
					CARatioDesc: "Sometimes we are missing detailed acceptance criteria",
				},
			},
			&mapper.Resource{
				Type:            "process",
				Title:           "Write Feature",
				Process:         "write-feature",
				PreviousProcess: "create-story",
				Description:     "The first story from the backlog is picked up and developed into a feature",
				NumberOfStaff:   1,
				Flow: &mapper.Flow{
					Out: []string{"git repository"},
					In:  []string{"issue tracker"},
				},
				Metrics: &mapper.Metrics{
					LeadTime:    "2d5m",
					ProcessTime: "2d",
					CARatio:     "95%",
					CARatioDesc: "Sometimes unit tests fill fail, or code quality tests",
				},
			},
			&mapper.Resource{
				Type:            "process",
				Title:           "Deploy Feature",
				Process:         "deploy-feature",
				PreviousProcess: "write-feature",
				Description:     "The story is tested then deployed",
				NumberOfStaff:   1,
				AutomatedFlow: &mapper.Flow{
					In: []string{"git repository"},
				},
				Metrics: &mapper.Metrics{
					LeadTime:    "20m",
					ProcessTime: "20m",
					CARatio:     "90%",
					CARatioDesc: "Occasionally migration tests will catch an error, or a deployment will fail",
				},
			},
			&mapper.Resource{
				Type:            "process",
				Title:           "Sign-off Feature",
				Process:         "sign-off-feature",
				PreviousProcess: "deploy-feature",
				Description:     "The feature is tested against the acceptance criteria and how to accept instructions",
				NumberOfStaff:   2,
				FlowBlockers:    "Only irregularly reviewed",
				Flow: &mapper.Flow{
					In: []string{"issue tracker"},
				},
				Metrics: &mapper.Metrics{
					LeadTime:    "10d",
					ProcessTime: "5m",
					CARatio:     "85%",
					CARatioDesc: "Sometimes there has been a misunderstanding about the feature should be completed, or an extra thing to add",
				},
			},
		},
	}

	exampleMap, _ := yaml.Marshal(valueSteam)

	fmt.Fprintln(c.App.Writer, string(exampleMap))

	return nil
}

func (actions *Actions) validate() (mapper.ValueStream, error) {
	fileData, err := ioutil.ReadFile(actions.Flags.Map)
	if err != nil {
		return mapper.ValueStream{}, cli.NewExitError(err, 1)
	}

	var valueStream mapper.ValueStream
	if err := yaml.Unmarshal(fileData, &valueStream); err != nil {
		errString := strings.Replace(err.Error(), "JSON", "YAML", -1)
		errString = strings.Replace(errString, "json", "yaml", -1)
		return mapper.ValueStream{}, cli.NewExitError(errString, 1)
	}
	if err := valueStream.Validate(); err != nil {
		marshelledErr, _ := json.MarshalIndent(err, "", "  ")
		return mapper.ValueStream{}, cli.NewExitError(string(marshelledErr), 1)
	}
	return valueStream, nil
}

func (actions *Actions) makeRequest(method, url string, body io.Reader) (int, []byte, error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return 0, nil, err
	}
	req.SetBasicAuth(actions.Flags.Username, actions.Flags.Password)
	resp, err := httpClient.Do(req)
	if err != nil {
		return 0, nil, err
	}
	defer resp.Body.Close()
	respBody, err := ioutil.ReadAll(resp.Body)
	return resp.StatusCode, respBody, err
}

func printErr(respBody []byte, writer io.Writer) {
	var errJSON errResp
	if err := json.Unmarshal(respBody, &errJSON); err == nil {
		fmt.Fprintln(writer, errJSON.Error)
	} else {
		fmt.Fprintln(writer, string(respBody))
	}
}

func (actions *Actions) versionQueryString() (string, error) {
	if actions.Flags.Version != "" {
		if err := mapper.ValidateVersion(actions.Flags.Version); err != nil {
			return "", cli.NewExitError(err, 1)
		}
		return fmt.Sprintf("?version=%s", actions.Flags.Version), nil
	}
	return "", nil
}

func (actions *Actions) appendVersionString(baseString string) string {
	if actions.Flags.Version != "" {
		return fmt.Sprintf("%s with version %s", baseString, actions.Flags.Version)
	}
	return baseString
}

func (actions *Actions) promptCLI() {
	actions.promptUsername()
	actions.promptPassword()
}

func (actions *Actions) promptUsername() {
	if actions.Flags.Username == "" {
		reader := bufio.NewReader(os.Stdin)
		fmt.Print("Enter Username: ")
		username, _ := reader.ReadString('\n')
		actions.Flags.Username = strings.TrimSpace(username)
	}
}

func (actions *Actions) promptPassword() {
	if actions.Flags.Password == "" {
		fmt.Print("Enter Password: ")
		bytePassword, _ := term.ReadPassword(int(syscall.Stdin))
		actions.Flags.Password = string(bytePassword)
		fmt.Println()
	}
}

func (actions *Actions) validGlobalFlags() bool {
	if actions.Flags.URI == "" {
		return false
	}
	actions.promptCLI()
	if actions.Flags.Username == "" ||
		actions.Flags.Password == "" {
		return false
	}
	return true
}

package vsmcli_test

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"bitbucket.org/armakuni/value-stream-mapping-ui/mapper"

	"github.com/ghodss/yaml"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/onsi/gomega/ghttp"

	cli "github.com/urfave/cli/v2"

	"bitbucket.org/armakuni/value-stream-mapping-cli"
)

var appHelp = `NAME:
   vsm - Interact with the value-stream-mapping-ui API

USAGE:
   vsm [global options] command [command options] [arguments...]

VERSION:
   dev

`

var exampleYaml = `customer: Mapper
resources:
- description: A user requests a specific feature
  metrics:
    c&aratio: 90%
    c&aratio_desc: Sometimes we need to ask for information on potential implementation
      to size
    leadtime: 30m
    processtime: 25m
  number_of_staff: 2
  process: feature-request
  title: Feature Request
  type: process
- description: Based on the feature request a story is created and put in a theme
  flow:
    out:
    - issue tracker
  metrics:
    c&aratio: 95%
    c&aratio_desc: Sometimes we are missing detailed acceptance criteria
    leadtime: 10m
    processtime: 10m
  number_of_staff: 1
  previous_process: feature-request
  process: create-story
  title: Create Story
  type: process
- description: The first story from the backlog is picked up and developed into a
    feature
  flow:
    in:
    - issue tracker
    out:
    - git repository
  metrics:
    c&aratio: 95%
    c&aratio_desc: Sometimes unit tests fill fail, or code quality tests
    leadtime: 2d5m
    processtime: 2d
  number_of_staff: 1
  previous_process: create-story
  process: write-feature
  title: Write Feature
  type: process
- automated_flow:
    in:
    - git repository
  description: The story is tested then deployed
  metrics:
    c&aratio: 90%
    c&aratio_desc: Occasionally migration tests will catch an error, or a deployment
      will fail
    leadtime: 20m
    processtime: 20m
  number_of_staff: 1
  previous_process: write-feature
  process: deploy-feature
  title: Deploy Feature
  type: process
- description: The feature is tested against the acceptance criteria and how to accept
    instructions
  flow:
    in:
    - issue tracker
  flow_blockers: Only irregularly reviewed
  metrics:
    c&aratio: 85%
    c&aratio_desc: Sometimes there has been a misunderstanding about the feature should
      be completed, or an extra thing to add
    leadtime: 10d
    processtime: 5m
  number_of_staff: 2
  previous_process: deploy-feature
  process: sign-off-feature
  title: Sign-off Feature
  type: process
title: Example Map
version: 0.1.0-current
working_day: 8h
working_week: 5d

`

var server *ghttp.Server

var _ = Describe("Actions#ValidateMap", func() {
	var (
		err         error
		bytesBuffer *bytes.Buffer
		actions     vsmcli.Actions
		fileName    = "fixtures/test.yml"
	)

	JustBeforeEach(func() {
		var buffer []byte
		bytesBuffer = bytes.NewBuffer(buffer)
		app := vsmcli.CreateCLI(bytesBuffer)
		context := cli.NewContext(app, nil, nil)
		err = actions.ValidateMap(context)
	})

	Context("when a file name is not provided", func() {
		BeforeEach(func() {
			actions = vsmcli.Actions{}
		})

		It("returns an error", func() {
			Ω(err).Should(MatchError("map flag must be set"))
			Ω(bytesBuffer.String()).Should(Equal(`NAME:
   validate-map - validate a value stream map

USAGE:
   validate-map [command options] [arguments...]

OPTIONS:
   --map value  the value-stream yaml file to validate
   
`))
		})
	})

	Context("when a file name is provided", func() {
		BeforeEach(func() {
			actions = vsmcli.Actions{
				Flags: vsmcli.Flags{
					Map: fileName,
				},
			}
		})

		Context("when the file does not exist", func() {
			It("returns an error", func() {
				Ω(err).Should(MatchError("open fixtures/test.yml: no such file or directory"))
				Ω(bytesBuffer.String()).Should(Equal(""))
			})
		})

		Context("when the file does exist", func() {
			var data []byte

			AfterEach(func() {
				Ω(os.Remove(fileName)).Should(Succeed())
			})

			Context("and it is not valid yaml", func() {
				BeforeEach(func() {
					data = []byte("not yaml")
					Ω(ioutil.WriteFile(fileName, data, os.ModePerm)).Should(Succeed())
				})

				It("returns an error", func() {
					Ω(err).Should(MatchError("error unmarshaling YAML: yaml: cannot unmarshal string into Go value of type mapper.ValueStream"))
					Ω(bytesBuffer.String()).Should(Equal(""))
				})
			})

			Context("and it is valid yaml", func() {
				Context("and the value stream map is invalid", func() {
					BeforeEach(func() {
						data = []byte(`---
key: value
`)
						Ω(ioutil.WriteFile(fileName, data, os.ModePerm)).Should(Succeed())
					})

					It("returns an error", func() {
						Ω(err).Should(MatchError(`{
  "resource-validation-errors": {
    "empty-resources-validation-error": "maps must have at least one resource"
  }
}`))
						Ω(bytesBuffer.String()).Should(Equal(""))
					})
				})

				Context("and the value stream map is valid", func() {
					BeforeEach(func() {
						data = []byte(`---
resources:
  - type: process
    process: test
`)
						Ω(ioutil.WriteFile(fileName, data, os.ModePerm)).Should(Succeed())
					})

					It("retruns nil", func() {
						Ω(err).ShouldNot(HaveOccurred())
						Ω(bytesBuffer.String()).Should(Equal(fmt.Sprintf("value stream map: %s is valid\n", fileName)))
					})
				})
			})
		})
	})
})

var _ = Describe("Actions#GetMap", func() {
	var (
		err         error
		bytesBuffer *bytes.Buffer
		actions     vsmcli.Actions
		username    = "test-user"
		password    = "test-password"
	)

	JustBeforeEach(func() {
		var buffer []byte
		bytesBuffer = bytes.NewBuffer(buffer)
		app := vsmcli.CreateCLI(bytesBuffer)
		context := cli.NewContext(app, nil, nil)
		err = actions.GetMap(context)
	})

	AfterEach(func() {
		actions = vsmcli.Actions{}
	})

	Context("when username is not provided", func() {
		It("returns an error", func() {
			Ω(err).Should(MatchError("global flags username, password and uri must be set"))
			Ω(bytesBuffer.String()).Should(Equal(appHelp))
		})
	})

	Context("when username is provided", func() {
		BeforeEach(func() {
			actions.Flags.Username = username
		})

		Context("and password is not provided", func() {
			It("returns an error", func() {
				Ω(err).Should(MatchError("global flags username, password and uri must be set"))
				Ω(bytesBuffer.String()).Should(Equal(appHelp))
			})
		})

		Context("and password is provided", func() {
			BeforeEach(func() {
				actions.Flags.Password = password
			})

			Context("and uri is not provided", func() {
				It("returns an error", func() {
					Ω(err).Should(MatchError("global flags username, password and uri must be set"))
					Ω(bytesBuffer.String()).Should(Equal(appHelp))
				})
			})

			Context("and uri is provided", func() {
				BeforeEach(func() {
					actions.Flags.URI = "127.0.0.1"
				})

				Context("and name is not provided", func() {
					It("returns an error", func() {
						Ω(err).Should(MatchError("name flag must be set"))
						Ω(bytesBuffer.String()).Should(Equal(`NAME:
   get-map - download a value stream map as yaml

USAGE:
   get-map [command options] [arguments...]

OPTIONS:
   --name value     the value-stream map name
   --version value  the value-stream map version
   
`))
					})
				})

				Context("and name is provided", func() {
					BeforeEach(func() {
						actions.Flags.Name = "test-map"
						server = ghttp.NewServer()
					})

					AfterEach(func() {
						server.Close()
					})

					Context("and version is not provided", func() {
						Context("and the URI is invalid", func() {
							It("returns an error", func() {
								Ω(err).Should(MatchError(`Get 127.0.0.1/v1/maps/test-map: unsupported protocol scheme ""`))
								Ω(bytesBuffer.String()).Should(Equal(""))
							})
						})

						Context("when the URI is valid", func() {
							Context("and the http request is not successful", func() {
								BeforeEach(func() {
									server.AppendHandlers(
										ghttp.CombineHandlers(
											ghttp.VerifyRequest("GET", "/v1/maps/test-map"),
											ghttp.VerifyBasicAuth(username, password),
											ghttp.RespondWith(http.StatusGone, "{}"),
										),
									)
									actions.Flags.URI = server.URL()
								})

								It("returns an error", func() {
									Ω(err).Should(MatchError("could not get value steam map for test-map"))
									Ω(bytesBuffer.String()).Should(Equal("\n"))
								})
							})

							Context("and the http request is successful", func() {
								BeforeEach(func() {
									server.AppendHandlers(
										ghttp.CombineHandlers(
											ghttp.VerifyRequest("GET", "/v1/maps/test-map"),
											ghttp.VerifyBasicAuth(username, password),
											ghttp.RespondWith(http.StatusOK, exampleMapJSON),
										),
									)
									actions.Flags.URI = server.URL()
								})

								It("returns the map", func() {
									Ω(err).ShouldNot(HaveOccurred())
									var valueStream mapper.ValueStream
									Ω(yaml.Unmarshal(bytesBuffer.Bytes(), &valueStream)).Should(Succeed())
									Ω(valueStream.Resources).Should(HaveLen(3))
									Ω(valueStream.Version).Should(Equal(""))
								})
							})
						})
					})

					Context("and version is provided", func() {
						Context("and version is not a semver", func() {
							BeforeEach(func() {
								actions.Flags.Version = "notSemver"
							})

							It("returns an error", func() {
								Ω(err).Should(MatchError("version must be semver compliant, was: notSemver"))
								Ω(bytesBuffer.String()).Should(Equal(""))
							})
						})

						Context("and version is a semver", func() {
							BeforeEach(func() {
								actions.Flags.Version = "0.0.1"
							})

							Context("and the http request is not successful", func() {
								BeforeEach(func() {
									server.AppendHandlers(
										ghttp.CombineHandlers(
											ghttp.VerifyRequest("GET", "/v1/maps/test-map", "version=0.0.1"),
											ghttp.VerifyBasicAuth(username, password),
											ghttp.RespondWith(http.StatusGone, "{}"),
										),
									)
									actions.Flags.URI = server.URL()
								})

								It("returns an error", func() {
									Ω(err).Should(MatchError("could not get value steam map for test-map with version 0.0.1"))
									Ω(bytesBuffer.String()).Should(Equal("\n"))
								})
							})

							Context("and the http request is successful", func() {
								BeforeEach(func() {
									server.AppendHandlers(
										ghttp.CombineHandlers(
											ghttp.VerifyRequest("GET", "/v1/maps/test-map", "version=0.0.1"),
											ghttp.VerifyBasicAuth(username, password),
											ghttp.RespondWith(http.StatusOK, exampleMapJSONVersion),
										),
									)
									actions.Flags.URI = server.URL()
								})

								It("returns the map", func() {
									Ω(err).ShouldNot(HaveOccurred())
									var valueStream mapper.ValueStream
									Ω(yaml.Unmarshal(bytesBuffer.Bytes(), &valueStream)).Should(Succeed())
									Ω(valueStream.Resources).Should(HaveLen(3))
									Ω(valueStream.Version).Should(Equal("0.0.1"))
								})
							})
						})
					})
				})
			})
		})
	})
})

var _ = Describe("Actions#SetMap", func() {
	var (
		err         error
		bytesBuffer *bytes.Buffer
		actions     vsmcli.Actions
		fileName    = "fixtures/test.yml"
		username    = "test-username"
		password    = "test-password"
	)

	JustBeforeEach(func() {
		var buffer []byte
		bytesBuffer = bytes.NewBuffer(buffer)
		app := vsmcli.CreateCLI(bytesBuffer)
		context := cli.NewContext(app, nil, nil)
		err = actions.SetMap(context)
	})

	AfterEach(func() {
		actions = vsmcli.Actions{}
	})

	Context("when username is not provided", func() {
		It("returns an error", func() {
			Ω(err).Should(MatchError("global flags username, password and uri must be set"))
			Ω(bytesBuffer.String()).Should(Equal(appHelp))
		})
	})

	Context("when username is provided", func() {
		BeforeEach(func() {
			actions.Flags.Username = username
		})

		Context("and password is not provided", func() {
			It("returns an error", func() {
				Ω(err).Should(MatchError("global flags username, password and uri must be set"))
				Ω(bytesBuffer.String()).Should(Equal(appHelp))
			})
		})

		Context("and password is provided", func() {
			BeforeEach(func() {
				actions.Flags.Password = password
			})

			Context("and uri is not provided", func() {
				It("returns an error", func() {
					Ω(err).Should(MatchError("global flags username, password and uri must be set"))
					Ω(bytesBuffer.String()).Should(Equal(appHelp))
				})
			})

			Context("and uri is provided", func() {
				BeforeEach(func() {
					actions.Flags.URI = "127.0.0.1"
				})

				Context("and name is not provided", func() {
					It("returns an error", func() {
						Ω(err).Should(MatchError("name and map flags must be set"))
						Ω(bytesBuffer.String()).Should(Equal(`NAME:
   set-map - set a value stream map from yaml

USAGE:
   set-map [command options] [arguments...]

OPTIONS:
   --name value  the value-stream map name
   --map value   the value-stream yaml file to set
   
`))
					})
				})

				Context("and name is provided", func() {
					BeforeEach(func() {
						actions.Flags.Name = "test-name"
					})

					Context("and map is not provided", func() {
						It("returns an error", func() {
							Ω(err).Should(MatchError("name and map flags must be set"))
							Ω(bytesBuffer.String()).Should(Equal(`NAME:
   set-map - set a value stream map from yaml

USAGE:
   set-map [command options] [arguments...]

OPTIONS:
   --name value  the value-stream map name
   --map value   the value-stream yaml file to set
   
`))
						})
					})
					Context("and map is provided", func() {
						var data []byte

						AfterEach(func() {
							Ω(os.Remove(fileName)).Should(Succeed())
						})

						BeforeEach(func() {
							actions.Flags.Map = fileName
						})

						Context("and map is invalid", func() {
							BeforeEach(func() {
								data = []byte(`---
key: value
`)
								Ω(ioutil.WriteFile(fileName, data, os.ModePerm)).Should(Succeed())
							})

							It("returns an error", func() {
								Ω(err).Should(MatchError(`{
  "resource-validation-errors": {
    "empty-resources-validation-error": "maps must have at least one resource"
  }
}`))
								Ω(bytesBuffer.String()).Should(Equal(""))
							})
						})

						Context("and map is valid", func() {
							BeforeEach(func() {
								data = []byte(`---
resources:
  - type: process
    process: test
`)
								Ω(ioutil.WriteFile(fileName, data, os.ModePerm)).Should(Succeed())
							})

							Context("when the http request is invalid", func() {
								It("returns an error", func() {
									Ω(err).Should(MatchError(`Put 127.0.0.1/v1/maps/test-name: unsupported protocol scheme ""`))
									Ω(bytesBuffer.String()).Should(Equal(""))
								})
							})

							Context("when the http request is valid", func() {
								BeforeEach(func() {
									server = ghttp.NewServer()
								})

								AfterEach(func() {
									server.Close()
								})

								Context("and the putting the map fails", func() {
									BeforeEach(func() {
										server.AppendHandlers(
											ghttp.CombineHandlers(
												ghttp.VerifyRequest("PUT", "/v1/maps/test-name"),
												ghttp.VerifyBasicAuth(username, password),
												ghttp.RespondWith(http.StatusInternalServerError, `{"error":"an error was returned"}`),
											),
										)
										actions.Flags.URI = server.URL()
									})

									It("returns an error", func() {
										Ω(err).Should(MatchError("could not set value steam map for test-name"))
										Ω(bytesBuffer.String()).Should(Equal("an error was returned\n"))
									})
								})

								Context("and putting the map is successful", func() {
									BeforeEach(func() {
										server.AppendHandlers(
											ghttp.CombineHandlers(
												ghttp.VerifyRequest("PUT", "/v1/maps/test-name"),
												ghttp.VerifyBasicAuth(username, password),
												ghttp.RespondWith(http.StatusNoContent, ""),
											),
										)
										actions.Flags.URI = server.URL()
									})

									It("sets the map", func() {
										Ω(err).ShouldNot(HaveOccurred())
									})
								})
							})
						})
					})
				})
			})
		})
	})
})

var _ = Describe("Actions#ListMapVersions", func() {
	var (
		err         error
		bytesBuffer *bytes.Buffer
		actions     vsmcli.Actions
		username    = "test-username"
		password    = "test-password"
	)

	JustBeforeEach(func() {
		var buffer []byte
		bytesBuffer = bytes.NewBuffer(buffer)
		app := vsmcli.CreateCLI(bytesBuffer)
		context := cli.NewContext(app, nil, nil)
		err = actions.ListMapVersions(context)
	})

	AfterEach(func() {
		actions = vsmcli.Actions{}
	})

	Context("when username is not provided", func() {
		It("returns an error", func() {
			Ω(err).Should(MatchError("global flags username, password and uri must be set"))
			Ω(bytesBuffer.String()).Should(Equal(appHelp))
		})
	})

	Context("when username is provided", func() {
		BeforeEach(func() {
			actions.Flags.Username = username
		})

		Context("and password is not provided", func() {
			It("returns an error", func() {
				Ω(err).Should(MatchError("global flags username, password and uri must be set"))
				Ω(bytesBuffer.String()).Should(Equal(appHelp))
			})
		})

		Context("and password is provided", func() {
			BeforeEach(func() {
				actions.Flags.Password = password
			})

			Context("and uri is not provided", func() {
				It("returns an error", func() {
					Ω(err).Should(MatchError("global flags username, password and uri must be set"))
					Ω(bytesBuffer.String()).Should(Equal(appHelp))
				})
			})

			Context("and uri is provided", func() {
				BeforeEach(func() {
					actions.Flags.URI = "127.0.0.1"
				})

				Context("and name is not provided", func() {
					It("returns an error", func() {
						Ω(err).Should(MatchError("name flag must be set"))
						Ω(bytesBuffer.String()).Should(Equal(`NAME:
   list-map-versions - list value-stream map versions for a map

USAGE:
   list-map-versions [command options] [arguments...]

OPTIONS:
   --name value  the value-stream map name
   
`))
					})
				})

				Context("and name is provided", func() {
					BeforeEach(func() {
						actions.Flags.Name = "test-map"
					})

					Context("and the URI is invalid", func() {
						It("returns an error", func() {
							Ω(err).Should(MatchError(`Get 127.0.0.1/v1/versions/test-map: unsupported protocol scheme ""`))
							Ω(bytesBuffer.String()).Should(Equal(""))
						})
					})

					Context("and the URI is valid", func() {
						BeforeEach(func() {
							server = ghttp.NewServer()
						})

						AfterEach(func() {
							server.Close()
						})

						Context("and the map does not exist", func() {
							BeforeEach(func() {
								server.AppendHandlers(
									ghttp.CombineHandlers(
										ghttp.VerifyRequest("GET", "/v1/versions/test-map"),
										ghttp.VerifyBasicAuth(username, password),
										ghttp.RespondWith(http.StatusInternalServerError, noMapJSON),
									),
								)
								actions.Flags.URI = server.URL()
							})

							It("returns an error", func() {
								Ω(err).Should(MatchError("could not get value steam map versions for test-map"))
								Ω(bytesBuffer.String()).Should(Equal("map with name: 'test-map' does not exist, cannot list versions\n"))
							})
						})

						Context("and the map does exist", func() {
							Context("without versions", func() {
								BeforeEach(func() {
									server.AppendHandlers(
										ghttp.CombineHandlers(
											ghttp.VerifyRequest("GET", "/v1/versions/test-map"),
											ghttp.VerifyBasicAuth(username, password),
											ghttp.RespondWith(http.StatusOK, noVersionsJSON),
										),
									)
									actions.Flags.URI = server.URL()
								})

								It("returns an error", func() {
									Ω(err).ShouldNot(HaveOccurred())
									Ω(bytesBuffer.String()).Should(Equal("map test-map does not have versions"))
								})
							})

							Context("with versions", func() {
								BeforeEach(func() {
									server.AppendHandlers(
										ghttp.CombineHandlers(
											ghttp.VerifyRequest("GET", "/v1/versions/test-map"),
											ghttp.VerifyBasicAuth(username, password),
											ghttp.RespondWith(http.StatusOK, versionsJSON),
										),
									)
									actions.Flags.URI = server.URL()
								})

								It("returns the versions", func() {
									Ω(err).ShouldNot(HaveOccurred())
									Ω(bytesBuffer.String()).Should(Equal(`0.0.1
0.1.1
1.0.0
`))
								})
							})
						})
					})
				})
			})
		})
	})
})

var _ = Describe("Actions#DeleteMap", func() {
	var (
		err         error
		bytesBuffer *bytes.Buffer
		actions     vsmcli.Actions
		username    = "test-username"
		password    = "test-password"
	)

	JustBeforeEach(func() {
		var buffer []byte
		bytesBuffer = bytes.NewBuffer(buffer)
		app := vsmcli.CreateCLI(bytesBuffer)
		context := cli.NewContext(app, nil, nil)
		err = actions.DeleteMap(context)
	})

	AfterEach(func() {
		actions = vsmcli.Actions{}
	})

	Context("when username is not provided", func() {
		It("returns an error", func() {
			Ω(err).Should(MatchError("global flags username, password and uri must be set"))
			Ω(bytesBuffer.String()).Should(Equal(appHelp))
		})
	})

	Context("when username is provided", func() {
		BeforeEach(func() {
			actions.Flags.Username = username
		})

		Context("and password is not provided", func() {
			It("returns an error", func() {
				Ω(err).Should(MatchError("global flags username, password and uri must be set"))
				Ω(bytesBuffer.String()).Should(Equal(appHelp))
			})
		})

		Context("and password is provided", func() {
			BeforeEach(func() {
				actions.Flags.Password = password
			})

			Context("and uri is not provided", func() {
				It("returns an error", func() {
					Ω(err).Should(MatchError("global flags username, password and uri must be set"))
					Ω(bytesBuffer.String()).Should(Equal(appHelp))
				})
			})

			Context("and uri is provided", func() {
				BeforeEach(func() {
					actions.Flags.URI = "127.0.0.1"
				})

				Context("and name is not provided", func() {
					It("returns an error", func() {
						Ω(err).Should(MatchError("name flag must be set"))
						fmt.Println("####################################")
						fmt.Println("####################################")
						fmt.Println("####################################")
						fmt.Println("####################################")
						fmt.Println("####################################")
						fmt.Println(bytesBuffer.String())
						Ω(bytesBuffer.String()).Should(Equal(`NAME:
   delete-map - delete a value stream map

USAGE:
   delete-map [command options] [arguments...]

OPTIONS:
   --name value     the value-stream map name
   --version value  the value-stream map version
   
`))
					})
				})

				Context("and name is provided", func() {
					BeforeEach(func() {
						actions.Flags.Name = "test-map"
						server = ghttp.NewServer()
					})

					AfterEach(func() {
						server.Close()
					})

					Context("and version is not provided", func() {
						Context("and the URI is invalid", func() {
							It("returns an error", func() {
								Ω(err).Should(MatchError(`Delete 127.0.0.1/v1/maps/test-map: unsupported protocol scheme ""`))
								Ω(bytesBuffer.String()).Should(Equal(""))
							})
						})

						Context("when the URI is valid", func() {
							Context("and the http request is not successful", func() {
								BeforeEach(func() {
									server.AppendHandlers(
										ghttp.CombineHandlers(
											ghttp.VerifyRequest("DELETE", "/v1/maps/test-map"),
											ghttp.VerifyBasicAuth(username, password),
											ghttp.RespondWith(http.StatusInternalServerError, deleteErrJSON),
										),
									)
									actions.Flags.URI = server.URL()
								})

								It("returns an error", func() {
									Ω(err).Should(MatchError("could not delete map test-map"))
									Ω(bytesBuffer.String()).Should(Equal("map with name: 'test-map' does not exist, cannot list versions\n"))
								})
							})

							Context("and the http request is successful", func() {
								BeforeEach(func() {
									server.AppendHandlers(
										ghttp.CombineHandlers(
											ghttp.VerifyRequest("DELETE", "/v1/maps/test-map"),
											ghttp.VerifyBasicAuth(username, password),
											ghttp.RespondWith(http.StatusOK, ""),
										),
									)
									actions.Flags.URI = server.URL()
								})

								It("returns the map", func() {
									Ω(err).ShouldNot(HaveOccurred())
									Ω(bytesBuffer.String()).Should(Equal("deleted map test-map\n"))
								})
							})
						})
					})

					Context("and version is provided", func() {
						Context("and version is not a semver", func() {
							BeforeEach(func() {
								actions.Flags.Version = "notSemver"
							})

							It("returns an error", func() {
								Ω(err).Should(MatchError("version must be semver compliant, was: notSemver"))
								Ω(bytesBuffer.String()).Should(Equal(""))
							})
						})

						Context("and version is a semver", func() {
							BeforeEach(func() {
								actions.Flags.Version = "0.0.1"
							})

							Context("and the http request is not successful", func() {
								BeforeEach(func() {
									server.AppendHandlers(
										ghttp.CombineHandlers(
											ghttp.VerifyRequest("DELETE", "/v1/maps/test-map", "version=0.0.1"),
											ghttp.VerifyBasicAuth(username, password),
											ghttp.RespondWith(http.StatusGone, deleteErrVersionJSON),
										),
									)
									actions.Flags.URI = server.URL()
								})

								It("returns an error", func() {
									Ω(err).Should(MatchError("could not delete map test-map with version 0.0.1"))
									Ω(bytesBuffer.String()).Should(Equal("Could not delete map 'test-map:0.0.1' as it did not exist, found other versions: [0.1.1, 1.0.0]\n"))
								})
							})

							Context("and the http request is successful", func() {
								BeforeEach(func() {
									server.AppendHandlers(
										ghttp.CombineHandlers(
											ghttp.VerifyRequest("DELETE", "/v1/maps/test-map", "version=0.0.1"),
											ghttp.VerifyBasicAuth(username, password),
											ghttp.RespondWith(http.StatusOK, ""),
										),
									)
									actions.Flags.URI = server.URL()
								})

								It("returns the map", func() {
									Ω(err).ShouldNot(HaveOccurred())
									Ω(bytesBuffer.String()).Should(Equal("deleted map test-map with version 0.0.1\n"))
								})
							})
						})
					})
				})
			})
		})
	})
})

var _ = Describe("#ListMaps", func() {
	var (
		err         error
		bytesBuffer *bytes.Buffer
		actions     vsmcli.Actions
		username    = "test-username"
		password    = "test-password"
	)

	JustBeforeEach(func() {
		var buffer []byte
		bytesBuffer = bytes.NewBuffer(buffer)
		app := vsmcli.CreateCLI(bytesBuffer)
		context := cli.NewContext(app, nil, nil)
		err = actions.ListMaps(context)
	})

	AfterEach(func() {
		actions = vsmcli.Actions{}
	})

	Context("when username is not provided", func() {
		It("returns an error", func() {
			Ω(err).Should(MatchError("global flags username, password and uri must be set"))
			Ω(bytesBuffer.String()).Should(Equal(appHelp))
		})
	})

	Context("when username is provided", func() {
		BeforeEach(func() {
			actions.Flags.Username = username
		})

		Context("and password is not provided", func() {
			It("returns an error", func() {
				Ω(err).Should(MatchError("global flags username, password and uri must be set"))
				Ω(bytesBuffer.String()).Should(Equal(appHelp))
			})
		})

		Context("and password is provided", func() {
			BeforeEach(func() {
				actions.Flags.Password = password
			})

			Context("and uri is not provided", func() {
				It("returns an error", func() {
					Ω(err).Should(MatchError("global flags username, password and uri must be set"))
					Ω(bytesBuffer.String()).Should(Equal(appHelp))
				})
			})

			Context("and uri is provided", func() {
				BeforeEach(func() {
					actions.Flags.URI = "127.0.0.1"
				})

				Context("and the URI is invalid", func() {
					It("returns an error", func() {
						Ω(err).Should(MatchError(`Get 127.0.0.1/v1/titles: unsupported protocol scheme ""`))
						Ω(bytesBuffer.String()).Should(Equal(""))
					})
				})

				Context("when the URI is valid", func() {
					var server *ghttp.Server

					BeforeEach(func() {
						server = ghttp.NewServer()
					})

					AfterEach(func() {
						server.Close()
					})
					Context("and the http request is not successful", func() {
						BeforeEach(func() {
							server.AppendHandlers(
								ghttp.CombineHandlers(
									ghttp.VerifyRequest("GET", "/v1/titles"),
									ghttp.VerifyBasicAuth(username, password),
									ghttp.RespondWith(http.StatusInternalServerError, getTitlesErrJSON),
								),
							)
							actions.Flags.URI = server.URL()
						})

						It("returns an error", func() {
							Ω(err).Should(MatchError("could not get maps"))
							Ω(bytesBuffer.String()).Should(Equal("Could not get titles\n"))
						})
					})

					Context("and the http request is successful", func() {
						BeforeEach(func() {
							server.AppendHandlers(
								ghttp.CombineHandlers(
									ghttp.VerifyRequest("GET", "/v1/titles"),
									ghttp.VerifyBasicAuth(username, password),
									ghttp.RespondWith(http.StatusOK, getTitlesJSON),
								),
							)
							actions.Flags.URI = server.URL()
						})

						It("lists the maps with corresponding titles", func() {
							Ω(err).ShouldNot(HaveOccurred())
							lines := strings.Split(bytesBuffer.String(), "\n")
							Ω(lines).Should(HaveLen(3))
							Ω(lines).Should(ContainElement("Name: test1, Title: An example test"))
							Ω(lines).Should(ContainElement("Name: test2, Title: another example"))
							Ω(lines).Should(ContainElement(""))
						})
					})
				})
			})
		})
	})
})

var _ = Describe("#ExampleMap", func() {
	var (
		err          error
		unmarshalErr error
		bytesBuffer  *bytes.Buffer
		actions      vsmcli.Actions
		valueStream  mapper.ValueStream
	)

	JustBeforeEach(func() {
		var buffer []byte
		bytesBuffer = bytes.NewBuffer(buffer)
		app := vsmcli.CreateCLI(bytesBuffer)
		context := cli.NewContext(app, nil, nil)
		err = actions.ExampleMap(context)

		unmarshalErr = yaml.Unmarshal(bytesBuffer.Bytes(), &valueStream)
	})

	AfterEach(func() {
		actions = vsmcli.Actions{}
	})

	It("Prints an example", func() {
		Ω(err).ShouldNot(HaveOccurred())
		Ω(bytesBuffer.String()).Should(Equal(exampleYaml))
	})

	It("It serializes into a valid vsm", func() {
		Ω(unmarshalErr).ShouldNot(HaveOccurred())
		Ω(valueStream.Validate()).ShouldNot(HaveOccurred())
	})
})

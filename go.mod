module bitbucket.org/armakuni/value-stream-mapping-cli

go 1.13

require (
	bitbucket.org/armakuni/value-stream-mapping-ui v0.0.0-20200120130216-5057e3cf823d
	github.com/ghodss/yaml v1.0.0
	github.com/onsi/ginkgo v1.11.0
	github.com/onsi/gomega v1.8.1
	github.com/urfave/cli/v2 v2.1.1
	golang.org/x/term v0.4.0
)

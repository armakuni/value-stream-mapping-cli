# value-stream-mapping-cli

To clone this repo:

```
if ! go get bitbucket.org/armakuni/value-stream-mapping-cli ; then
    mkdir -p "$GOPATH/src/bitbucket.org/armakuni/value-stream-mapping-cli"
    git clone git@bitbucket.org:armakuni/value-stream-mapping-cli.git "$GOPATH/src/bitbucket.org/armakuni/value-stream-mapping-cli"
fi

cd "$GOPATH/src/bitbucket.org/armakuni/value-stream-mapping-cli"
```

**Note**: `build` and `install` use `go get` on a private github repo. To make this use your SSH keys instead of HTTPS please run:

```sh
git config --global url.git@bitbucket.org:.insteadOf https://bitbucket.org/
```

## Install

* Install [mercurial](https://www.mercurial-scm.org/downloads)

then run

`make install`

## Use

The CLI should be self documenting, try running:

`vsm --help`

## Build

`make build`

**Note**: This builds the binary to: `./vsm/vsm`

## Development

### Dependencies

`make prepare-local`

### Test

`make test`

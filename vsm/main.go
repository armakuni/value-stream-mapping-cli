package main

import (
	"os"

	vsmcli "bitbucket.org/armakuni/value-stream-mapping-cli"
)

// BuildVersion is the version of the application. Set a build time.
var BuildVersion string

func main() {
	app := vsmcli.CreateCLI(os.Stdout)

	if BuildVersion != "" {
		app.Version = BuildVersion
	}

	app.Run(os.Args)
}

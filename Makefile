help:
	@grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: prepare-local ## build the vsm binary
	@(cd vsm && go build)

prepare-local: ## install tools and deps for running locally
	@GOPRIVATE=bitbucket.org/armakuni/value-stream-mapping-ui go mod tidy

test: ## run tests
	@ginkgo -r -cover

install: prepare-local ## install the binary to $GOPATH/bin
	@go get bitbucket.org/armakuni/value-stream-mapping-cli/vsm

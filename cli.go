package vsmcli

import (
	"io"

	cli "github.com/urfave/cli/v2"
)

// CreateCLI creates a *cli.App
func CreateCLI(writer io.Writer) *cli.App {
	var actions Actions
	app := cli.NewApp()
	app.Writer = writer
	app.EnableBashCompletion = true
	app.Name = "vsm"
	app.HelpName = "vsm"
	app.Usage = "Interact with the value-stream-mapping-ui API"
	app.Version = "dev"
	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:        "username, u",
			Usage:       "your username for value-stream-mapping-ui",
			EnvVars:     []string{"VSM_USERNAME"},
			Destination: &actions.Flags.Username,
		},
		&cli.StringFlag{
			Name:        "password, p",
			Usage:       "your password for value-stream-mapping-ui",
			EnvVars:     []string{"VSM_PASSWORD"},
			Destination: &actions.Flags.Password,
		},
		&cli.StringFlag{
			Name:        "uri",
			Usage:       "the uri of your value-stream-mapping-ui server",
			EnvVars:     []string{"VSM_URI"},
			Destination: &actions.Flags.URI,
		},
	}
	app.Commands = []*cli.Command{
		{
			Name:     "validate-map",
			HelpName: "validate-map",
			Aliases:  []string{"vm"},
			Usage:    "validate a value stream map",
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name:        "map, m",
					Usage:       "the value-stream yaml file to validate",
					Destination: &actions.Flags.Map,
				},
			},
			Action: actions.ValidateMap,
		},
		{
			Name:     "get-map",
			HelpName: "get-map",
			Usage:    "download a value stream map as yaml",
			Aliases:  []string{"gm"},
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name:        "name, n",
					Usage:       "the value-stream map name",
					Destination: &actions.Flags.Name,
				},
				&cli.StringFlag{
					Name:        "version, v",
					Usage:       "the value-stream map version",
					Destination: &actions.Flags.Version,
				},
			},
			Action: actions.GetMap,
		},
		{
			Name:     "set-map",
			HelpName: "set-map",
			Usage:    "set a value stream map from yaml",
			Aliases:  []string{"sm"},
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name:        "name, n",
					Usage:       "the value-stream map name",
					Destination: &actions.Flags.Name,
				},
				&cli.StringFlag{
					Name:        "map, m",
					Usage:       "the value-stream yaml file to set",
					Destination: &actions.Flags.Map,
				},
			},
			Action: actions.SetMap,
		},
		{
			Name:     "list-map-versions",
			HelpName: "list-map-versions",
			Usage:    "list value-stream map versions for a map",
			Aliases:  []string{"lmv"},
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name:        "name, n",
					Usage:       "the value-stream map name",
					Destination: &actions.Flags.Name,
				},
			},
			Action: actions.ListMapVersions,
		},
		{
			Name:     "delete-map",
			HelpName: "delete-map",
			Usage:    "delete a value stream map",
			Aliases:  []string{"dm"},
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name:        "name, n",
					Usage:       "the value-stream map name",
					Destination: &actions.Flags.Name,
				},
				&cli.StringFlag{
					Name:        "version, v",
					Usage:       "the value-stream map version",
					Destination: &actions.Flags.Version,
				},
			},
			Action: actions.DeleteMap,
		},
		{
			Name:     "list-maps",
			HelpName: "list-maps",
			Usage:    "list value-stream maps and their related titles",
			Aliases:  []string{"l"},
			Action:   actions.ListMaps,
		},
		{
			Name:     "example-map",
			HelpName: "example-map",
			Usage:    "print an example value stream map",
			Aliases:  []string{"e"},
			Action:   actions.ExampleMap,
		},
	}

	return app
}
